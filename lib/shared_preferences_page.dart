import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_prefences.dart'
class SharedPreferencesPage extends StatefulWidget {
  @override
  _SharedPreferencesPageState createState() => _SharedPreferencesPageState();
}

class _SharedPreferencesPageState extends State<SharedPreferencesPage> {
  String _currentValue = "";

  String element = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DemoApp"),
      ),
      body: Column(
        children: [
          TextField(
            onChanged: (value) {
              _currentValue = value;
            },
          ),
          RaisedButton(
            onPressed: () async {
              if (_currentValue != "") {
                element ??= await addToSP(_currentValue);
                setState(() {});
              }
            },
            child: Text("Agregar Elemento"),
          ),
          Container(
            height: 300,
            child: Text(element),
          )
        ],
      ),
    );
  }
}

Future<String> addToSP(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('contents', value);
  String contents = prefs.getString('contents') ?? "";
  return contents;
}
