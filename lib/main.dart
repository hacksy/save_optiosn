import 'package:almacenamiento/shared_preferences_page.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

void main() {
  runApp(MaterialApp(home: SharedPreferencesPage()));
  //  runApp(MaterialApp(home: DemoApp()));
}

class DemoApp extends StatefulWidget {
  @override
  _DemoAppState createState() => _DemoAppState();
}

class _DemoAppState extends State<DemoApp> {
  List<String> elements = List();

  String _currentValue = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DemoApp"),
      ),
      body: Column(
        children: [
          TextField(
            onChanged: (value) {
              _currentValue = value;
            },
          ),
          RaisedButton(
            onPressed: () async {
              if (_currentValue != "") {
                elements = await addElement(_currentValue);
                setState(() {});
              }
            },
            child: Text("Agregar Elemento"),
          ),
          Container(
            height: 300,
            child: ListView.builder(
                itemCount: elements.length,
                itemBuilder: (BuildContext context, int pos) {
                  return ListTile(
                    title: Text(elements[pos]),
                  );
                }),
          )
        ],
      ),
    );
  }
}

Future<List<String>> addElement(String contents) async {
  var databasesPath = await getDatabasesPath();
  String path = databasesPath + "/" + "almacenamiento.db";
  Database database = await openDatabase(
    path,
    version: 5,
    onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Test(id INTEGER PRIMARY KEY, name TEXT)');
    },
  );

  await database.rawInsert('INSERT INTO Test(name) values (?)', [contents]);
  List<Map> myResults = await database.rawQuery("SELECT * FROM Test");
  List<String> results = List<String>();
  myResults.forEach((element) {
    results.add(element['name']);
  });
  database.close();
  return results;
}
